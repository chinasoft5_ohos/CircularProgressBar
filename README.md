# CircularProgressBar

#### 项目介绍
- 项目名称：CircularProgressBar
- 所属系列：openharmony的第三方组件适配移植
- 功能：以最简单的方式实现一个圆形的进度条
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：3.1.0

#### 效果演示
![](/preview/preview.gif)

#### 安装教程
1.在项目根目录下的build.gradle文件中，
```
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/releases/'
    }
}
```

2.在entry模块的build.gradle文件中，
```
dependencies {
    implementation 'com.gitee.chinasoft_ohos:circularprogressbar:1.0.0'   
}
```


在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

**XML**

```xml
<com.mikhaellopez.circularprogressbar.CircularProgressBar
    ohos:id="$+id:circularProgressBar"
    ohos:height="match_content"
    ohos:width="match_content"
    app:cpb_background_progressbar_color="#b6bbd8"
    app:cpb_background_progressbar_width="5vp"
    app:cpb_progress_direction="1"
    app:cpb_progressbar_color="#3f51b5"
    app:cpb_progressbar_width="10vp"
    app:cpb_round_border="false" />
```

在XML中使用以下属性来更改CircularProgressBar。

| Properties                                       | Type         | Default               |
| ------------------------------------------------ | ------------ | --------------------- |
| `app:cpb_progress`                               | integer      | 0                     |
| `app:cpb_progress_max`                           | integer      | 100                   |
| `app:cpb_indeterminate_mode`                     | boolean      | false                 |
| `app:cpb_progressbar_color`                      | color        | BLACK                 |
| `app:cpb_progressbar_color_start`                | color        | cpb_progressbar_color |
| `app:cpb_progressbar_color_end`                  | color        | cpb_progressbar_color |
| `app:cpb_progressbar_color_direction`            | 1, 2, 3 or 4 | 1                     |
| `app:cpb_progressbar_width`                      | dimension    | 7vp                   |
| `app:cpb_background_progressbar_color`           | color        | GRAY                  |
| `app:cpb_background_progressbar_color_start`     | color        | GRAY                  |
| `app:cpb_background_progressbar_color_end`       | color        | GRAY                  |
| `app:cpb_background_progressbar_color_direction` | 1, 2, 3 or 4 | 1                     |
| `app:cpb_background_progressbar_width`           | dimension    | 3vp                   |
| `app:cpb_round_border`                           | boolean      | false                 |
| `app:cpb_start_angle`                            | float        | 0f (=top)             |
| `app:cpb_progress_direction`                     | 1 or 2       | 1              |

**JAVA**

```java
CircularProgressBar circularProgressBar = (CircularProgressBar) findComponentById(ResourceTable.Id_circularProgressBar);
// Set Progress
circularProgressBar.setProgress(65f);
// or with animation
circularProgressBar.setProgressWithAnimation(65f, 1000L, null, null); // =1s

// Set Progress Max
circularProgressBar.setProgressMax(200f);

// Set ProgressBar Color
circularProgressBar.setProgressBarColor(Color.BLACK);
// or with gradient
circularProgressBar.setProgressBarColorStart(Color.GRAY);
circularProgressBar.setProgressBarColorEnd(Color.RED);
circularProgressBar.setProgressBarColorDirection(CircularProgressBar.GradientDirection.TOP_TO_BOTTOM);

// Set background ProgressBar Color
circularProgressBar.setBackgroundProgressBarColor(Color.GRAY);
// or with gradient
circularProgressBar.setBackgroundProgressBarColorStart(Color.WHITE);
circularProgressBar.setBackgroundProgressBarColorEnd(Color.RED);
circularProgressBar.setBackgroundProgressBarColorDirection(CircularProgressBar.GradientDirection.TOP_TO_BOTTOM);

// Set Width
circularProgressBar.setProgressBarWidth(7f); // in VP
circularProgressBar.setBackgroundProgressBarWidth(3f); // in VP

// Other
circularProgressBar.setRoundBorder(true);
circularProgressBar.setStartAngle(180f);
circularProgressBar.setProgressDirection(CircularProgressBar.ProgressDirection.TO_RIGHT);
```

**Listener**

```java
circularProgressBar.setOnIndeterminateModeChangeListener(isEnable -> {
    // Do something
});
circularProgressBar.setOnProgressChangeListener(progress -> {
    // Do something
});
```

## 测试信息
CodeCheck代码测试无异常  

CloudTest代码测试无异常  

病毒安全检测通过  

当前版本demo功能与原组件基本无差异  

## 版本迭代
- 1.0.0

## 版权和许可信息  

CircularProgressBar by Lopez Mikhael is licensed under a Apache License 2.0.Based on a work at Pedramrn/CircularProgressBar.
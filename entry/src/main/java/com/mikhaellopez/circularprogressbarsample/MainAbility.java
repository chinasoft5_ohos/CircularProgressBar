/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mikhaellopez.circularprogressbarsample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Slider;
import ohos.agp.components.Switch;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;
import ohos.bundle.AbilityInfo;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

/**
 * 首页
 *
 * @since 2021-07-16
 */
public class MainAbility extends Ability implements Slider.ValueChangedListener {
    private static final int GREEN = -7617718;
    private static final int CYAN = -16728876;
    private static final int YELLOW = -5317;
    private static final int RED = -2937298;
    private static final int PURPLE = -10011977;
    private static final int BLUE = -12627531;
    private int[] colors = {GREEN, CYAN, YELLOW, RED, PURPLE, BLUE};

    private CircularProgressBar circularProgressBar;
    private Slider sliderProgress;
    private Slider sliderStartAngle;
    private Slider sliderStrokeWidth;
    private Slider sliderBackgroundStrokeWidth;
    private Switch switchRoundBorder;
    private Switch switchProgressDirection;
    private Switch switchIndeterminateMode;
    private IndicatorSeekBar indicatorSeekBar;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setNavigationBarColor(getColor(ResourceTable.Color_primary));
        getWindow().setStatusBarColor(getColor(ResourceTable.Color_primary_dark));
        setUIContent(ResourceTable.Layout_ability_main);

        circularProgressBar = (CircularProgressBar) findComponentById(ResourceTable.Id_circularProgressBar);

        // Set Init progress with animation
        circularProgressBar.setProgressWithAnimation(65f, 1000L, null, null);

        // Update circularProgressBar
        sliderProgress = (Slider) findComponentById(ResourceTable.Id_sliderProgress);
        sliderStartAngle = (Slider) findComponentById(ResourceTable.Id_sliderStartAngle);
        sliderStrokeWidth = (Slider) findComponentById(ResourceTable.Id_sliderStrokeWidth);
        sliderBackgroundStrokeWidth = (Slider) findComponentById(ResourceTable.Id_sliderBackgroundStrokeWidth);
        switchRoundBorder = (Switch) findComponentById(ResourceTable.Id_switchRoundBorder);
        switchProgressDirection = (Switch) findComponentById(ResourceTable.Id_switchProgressDirection);

        // Indeterminate Mode
        switchIndeterminateMode = (Switch) findComponentById(ResourceTable.Id_switchIndeterminateMode);

        indicatorSeekBar = (IndicatorSeekBar) findComponentById(ResourceTable.Id_indicator);
        indicatorSeekBar.customSectionTrackColor(colorIntArr -> {
            System.arraycopy(colors, 0, colorIntArr, 0, colors.length);
            return true;
        });
        setListener();
    }

    private void setListener() {
        sliderProgress.setValueChangedListener(this);
        sliderStartAngle.setValueChangedListener(this);
        sliderStrokeWidth.setValueChangedListener(this);
        sliderBackgroundStrokeWidth.setValueChangedListener(this);
        switchRoundBorder.setCheckedStateChangedListener((absButton, isChecked) ->
            circularProgressBar.setRoundBorder(isChecked));
        switchProgressDirection.setCheckedStateChangedListener((absButton, isChecked) ->
            circularProgressBar.setProgressDirection(isChecked ? CircularProgressBar.ProgressDirection.TO_RIGHT
                : CircularProgressBar.ProgressDirection.TO_LEFT));
        switchIndeterminateMode.setCheckedStateChangedListener((absButton, isChecked) ->
            circularProgressBar.setIndeterminateMode(isChecked));
        circularProgressBar.setOnIndeterminateModeChangeListener(isEnable ->
            switchIndeterminateMode.setChecked(isEnable));
        indicatorSeekBar.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                int position = (int) seekParams.progressFloat;
                position = Math.min(position, 5);
                circularProgressBar.setProgressBarColor(new Color(colors[position]));
                circularProgressBar.setBackgroundProgressBarColor(new Color(adjustAlpha(colors[position], 0.3f)));
                sliderProgress.setEnabled(false);
                sliderStartAngle.setEnabled(false);
                sliderStrokeWidth.setEnabled(false);
                sliderBackgroundStrokeWidth.setEnabled(false);
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar indicatorSeekBar) {
            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar indicatorSeekBar) {
                sliderProgress.setEnabled(true);
                sliderStartAngle.setEnabled(true);
                sliderStrokeWidth.setEnabled(true);
                sliderBackgroundStrokeWidth.setEnabled(true);
            }
        });
    }

    @Override
    public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
        switch (slider.getId()) {
            case ResourceTable.Id_sliderProgress:
                circularProgressBar.setProgress(slider.getProgress());
                break;
            case ResourceTable.Id_sliderStartAngle:
                circularProgressBar.setStartAngle(slider.getProgress());
                break;
            case ResourceTable.Id_sliderStrokeWidth:
                circularProgressBar.setProgressBarWidth(slider.getProgress());
                break;
            case ResourceTable.Id_sliderBackgroundStrokeWidth:
                circularProgressBar.setBackgroundProgressBarWidth(slider.getProgress());
                break;
            default:
                break;
        }
    }

    @Override
    public void onTouchStart(Slider slider) {
    }

    @Override
    public void onTouchEnd(Slider slider) {
    }

    /**
     * Transparent the given progressBarColor by the factor
     * The more the factor closer to zero the more the progressBarColor gets transparent
     *
     * @param color The progressBarColor to transparent
     * @param factor 1.0f to 0.0f
     * @return int - A transplanted progressBarColor
     */
    private int adjustAlpha(int color, float factor) {
        RgbColor rgbColor = RgbColor.fromArgbInt(color);
        rgbColor.setAlpha((int) (255 * factor));
        return rgbColor.asArgbInt();
    }

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        if (circularProgressBar != null) {
            circularProgressBar.setProgress(0f);
            circularProgressBar.setProgressWithAnimation(65f, 1000L, null, null);
        }
    }
}
